State Examples
==============

Let's try some more examples.

Instance Example
++++++++++++++++

Here's a simple example of creating a VM instance on AWS. For **subnet_id** and
**image_id** choose a value that's valid in the region you're using.

You can run the following command to get a list of available subnets:

.. code-block:: bash

    idem describe aws.ec2.subnet


You can also change the **instance_type** to a larger instance type, if you
prefer.

You can add any tags you want to the list under **tags**.

.. code-block:: yaml

    create_instance:
      aws.ec2.instance.present:
        - subnet_id: subnet-caefe68c
        - image_id: ami-0a149ac60ba7e9eca
        - instance_type: t2.nano
        - tags:
            Name: test-idem-cloud-demo
            owner: Tom

Create a text file with the above contents at a location such as
`~/instance_test.sls`. Then we can use that file to create our VM as we've
described.

.. code-block:: bash

    idem state ~/instance_test.sls

Output

.. code-block::

    --------
          ID: create_instance
    Function: aws.ec2.instance.present
      Result: True
     Comment: ("Created 'create_instance'", "Instance 'create_instance' already exists")
     Changes:
    new:
        ----------
        name:
            i-0256638454e8f9fb0
        resource_id:
            i-0256638454e8f9fb0
        image_id:
            ami-0a149ac60ba7e9eca
        instance_type:
            t2.nano
        volume_attachments:
            ----------
            /dev/sda1:
                vol-02a90ca0f32ca3d83
        block_device_mappings:
            |_
              ----------
              DeviceName:
                  /dev/sda1
              Ebs:
                  ----------
                  AttachTime:
                      2022-09-29 22:11:03+00:00
                  DeleteOnTermination:
                      True
                  Status:
                      attaching
                  VolumeId:
                      vol-02a90ca0f32ca3d83
        ebs_optimized:
            False
        subnet_id:
            subnet-caefe68c
        network_interfaces:
            |_
              ----------
              AssociatePublicIpAddress:
                  False
              DeleteOnTermination:
                  True
              Description:
              DeviceIndex:
                  0
              Groups:
                  - sg-4ce95029
              InterfaceType:
                  interface
              Ipv6Addresses:
              PrivateIpAddresses:
                  |_
                    ----------
                    Primary:
                        True
                    PrivateIpAddress:
                        172.31.4.47
              SubnetId:
                  subnet-caefe68c
              NetworkCardIndex:
                  0
        monitoring_enabled:
            False
        root_device_name:
            /dev/sda1
        client_token:
            3bf80009-7315-43f3-b6a6-5447fb97cde9
        product_codes:
        source_dest_check:
            True
        running:
            False
        private_ip_address:
            172.31.4.47
        reservation_id:
            r-0c26370a25f7ea773
        owner_id:
            812499087191
        availability_zone:
            us-west-2c
        group_name:
        tenancy:
            default
        disable_api_termination:
            False
        tags:
            ----------
            Name:
                test-idem-cloud-demo
            owner:
                Tom
        iam_profile_arn:
            ----------
        instance_initiated_shutdown_behavior:
            stop
        auto_recovery_enabled:
            True
        sriov_net_support:
            simple
        nitro_enclave_enabled:
            False
        license_arns:
        hibernation_enabled:
            False
        valid_until:
        cpu_credits:
            standard
        cpu_core_count:
            1
        cpu_threads_per_core:
            1
        http_tokens:
            optional
        http_put_response_hop_limit:
            1
        http_endpoint_enabled:
            True
        http_protocol_ipv6_enabled:
            False
        metadata_tags_enabled:
            False
        hostname_type:
            ip-name
        enable_resource_name_dns_a_record:
            False
        enable_resource_name_dns_aaaa_record:
            False
        capacity_reservation_preference:
            open
        bootstrap:


    present: 1 created successfully

If you go look at your AWS console, you'll see that a new VM named
`test-idem-cloud-demo` (or whatever name you changed the tag to) has been
created.


Remove Instance
+++++++++++++++

If you want to delete that VM with Idem, you can do it like this:


.. code-block:: bash

    idem state ~/instance_test.sls --invert

Output:

.. code-block::

    --------
          ID: create_instance
    Function: aws.ec2.instance.absent
      Result: True
     Comment: ("Not explicitly supported keyword argument: 'subnet_id'", "Not explicitly supported keyword argument: 'image_id'", "Not explicitly supported keyword argument: 'instance_type'", "Not explicitly supported keyword argument: 'tags'", "'create_instance' already terminated")
     Changes:
    old:
        ----------
        name:
            i-0256638454e8f9fb0
        resource_id:
            i-0256638454e8f9fb0
        image_id:
            ami-0a149ac60ba7e9eca
        instance_type:
            t2.nano
        volume_attachments:
            ----------
            /dev/sda1:
                vol-02a90ca0f32ca3d83
        block_device_mappings:
            |_
              ----------
              DeviceName:
                  /dev/sda1
              Ebs:
                  ----------
                  AttachTime:
                      2022-09-29 22:11:03+00:00
                  DeleteOnTermination:
                      True
                  Status:
                      attaching
                  VolumeId:
                      vol-02a90ca0f32ca3d83
        ebs_optimized:
            False
        subnet_id:
            subnet-caefe68c
        network_interfaces:
            |_
              ----------
              AssociatePublicIpAddress:
                  False
              DeleteOnTermination:
                  True
              Description:
              DeviceIndex:
                  0
              Groups:
                  - sg-4ce95029
              InterfaceType:
                  interface
              Ipv6Addresses:
              PrivateIpAddresses:
                  |_
                    ----------
                    Primary:
                        True
                    PrivateIpAddress:
                        172.31.4.47
              SubnetId:
                  subnet-caefe68c
              NetworkCardIndex:
                  0
        monitoring_enabled:
            False
        root_device_name:
            /dev/sda1
        client_token:
            3bf80009-7315-43f3-b6a6-5447fb97cde9
        product_codes:
        source_dest_check:
            True
        running:
            False
        private_ip_address:
            172.31.4.47
        reservation_id:
            r-0c26370a25f7ea773
        owner_id:
            812499087191
        availability_zone:
            us-west-2c
        group_name:
        tenancy:
            default
        disable_api_termination:
            False
        tags:
            ----------
            Name:
                test-idem-cloud-demo
            owner:
                Tom
        iam_profile_arn:
            ----------
        instance_initiated_shutdown_behavior:
            stop
        auto_recovery_enabled:
            True
        sriov_net_support:
            simple
        nitro_enclave_enabled:
            False
        license_arns:
        hibernation_enabled:
            False
        valid_until:
        cpu_credits:
            standard
        cpu_core_count:
            1
        cpu_threads_per_core:
            1
        http_tokens:
            optional
        http_put_response_hop_limit:
            1
        http_endpoint_enabled:
            True
        http_protocol_ipv6_enabled:
            False
        metadata_tags_enabled:
            False
        hostname_type:
            ip-name
        enable_resource_name_dns_a_record:
            False
        enable_resource_name_dns_aaaa_record:
            False
        capacity_reservation_preference:
            open
        bootstrap:


    absent: 1 deleted successfully
