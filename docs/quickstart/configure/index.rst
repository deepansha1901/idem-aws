Configure idem-aws
==================

Credential management in Idem is handled by the Idem  **acct** module. Details
about this module can be found on the `acct gitlab repo README
<https://gitlab.com/vmware/idem/acct>`_. By default the `idem-aws` acct plugin
will use the default AWS configuration that the logged in user already has
configured.

In this Quickstart we're going to provide the **AWS Access Key ID** and **AWS
Secret Access Key** to the AWS cli app. There are other ways to provide access
through IAM roles. Please refer to the official `AWS Best Practices for
Managing AWS access keys document.
<https://docs.aws.amazon.com/general/latest/gr/aws-access-keys-best-practices.html>`_

If you have already configured your environment with access to your AWS account
you can skip to the :doc:`Idem Command page.  </quickstart/commands/index>`


Install AWS CLI
+++++++++++++++

If you don't already have credentials set up you can install the AWS CLI to set
them up.

For details, follow the official `AWS CLI instructions
<https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html>`_.
For convenience we've included the instructions below.


.. tab-set::

    .. tab-item:: Linux
        :name: linux-aws-cli

        For Linux

        .. code-block:: bash

           curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
           unzip awscliv2.zip
           sudo ./aws/install

        Test your installation:

        .. code-block:: bash

            aws --version

    .. tab-item:: macOS
        :name: macos-aws-cli

        .. code-block:: bash

            curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
            sudo installer -pkg AWSCLIV2.pkg -target /

        Test your installation:

        .. code-block:: bash

            aws --version

    .. tab-item:: Windows
        :name: windows-aws-cli

        .. code-block:: bash

            msiexec.exe /i https://awscli.amazonaws.com/AWSCLIV2.msi

        To confirm the installation, opent the *Start* menu, search for `cmd`
        to open a command prompt window, and at the command prompt use the `aws
        --version` command.

        .. code-block:: bash

            C:\> aws --version
            aws-cli/2.7.24 Python/3.8.8 Windows/10 exe/AMD64 prompt/off


Needed AWS Creds
++++++++++++++++

You'll need the following AWS Credential info:

* AWS Access Key ID
* AWS Secret Access Key
* Your default region name you wish to use. (such as **us-west-2**)

Configure AWS
+++++++++++++

Once you've got the AWS ClI installed you can run the following command and
follow the prompts:

.. code-block:: bash

    aws configure



Here is some more info regarding credentials: `Boto docs <https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html>`_
