import copy

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_update_instance_type(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)
    state["instance_type"] = "t2.micro"

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert ret["changes"]["new"]["instance_type"] == "t2.micro"
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_reset_instance_type(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)
    state["instance_type"] = None

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert not ret["changes"].get("new", {}).get("instance_type")
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]
